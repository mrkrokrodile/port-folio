<!DOCTYPE html>
<html lang="fr">


<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="menu.css">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="projet.css">
  <link rel="stylesheet" href="background.css">
  <link rel="stylesheet" href="competence.css">
  <link href="https://fonts.googleapis.com/css2?family=Audiowide&display=swap" rel="stylesheet">



  <title>Gaëtan Beurel</title>
</head>

<body>
  <header>
    <!-- Le HTML du Menu -->

    <div id="mySidenav" class="sidenav">
      <div class="menu-content">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a style="margin-right:-50px;" href="#No1">
          <div onmouseover="afficher_texte1()" onmouseout="masquer_texte1()" id="circle1" class="circle"></div><span
            Id="ghost_text_1">Intro</span>
        </a>
        <div id="line"></div>

        <a style="margin-left: -150px;" href="#No2">
          <span Id="ghost_text_2">Expériences</span>
          <div onmouseover="afficher_texte2()" onmouseout="masquer_texte2()" id="circle2" class="circle"></div>
        </a>

        <div class="line-right"></div>

        <a style="margin-left:110px" href="#No3" class="right">
          <div onmouseover="afficher_texte3()" onmouseout="masquer_texte3()" id="circle3" class="circle"></div><span
            Id="ghost_text_3">Skills</span>
        </a>

        <div class="line-left"></div>

        <a style="margin-left: -150px;" href="#No4" class="left">
          <span Id="ghost_text_4">Formations</span>
          <div onmouseover="afficher_texte4()" onmouseout="masquer_texte4()" id="circle4" class="circle"></div>
        </a>

        <div class="line-right"></div>

        <a style="margin-left: 155px;" href="#No5" class="right">
          <div onmouseover="afficher_texte5()" onmouseout="masquer_texte5()" id="circle5" class="circle"></div><span
            Id="ghost_text_5">Projets</span>
        </a>

        <div class="line-left"></div>

        <a style="margin-right: 165px" href="#No6" class="left">
          <span Id="ghost_text_6">Contact</span>
          <div onmouseover="afficher_texte6()" onmouseout="masquer_texte6()" id="circle6" class="circle"></div>
        </a>

      </div>

    </div>

    <!-- BOUTON DU MENU -->

    <div id="rocket">
      <span style="font-size:30px;cursor:pointer" onclick="openNav()"><img id="fusée" src="img/fusée2.png"
          alt=""></span>
    </div>


    <!-- IMAGE INTRO -->

    <div class="cadreIntro">
      <div id='stars'></div>
      <div id='stars2'></div>
      <div id='stars3'></div>

      <div class="title">

        <div id="container_photo">
          <img id="photo" src="img/image.png" alt="identité">
        </div>
        <div id="introTitre">
          <h1>Gaëtan Beurel</h1>
        </div>

  </header>

  <main>







    <!-------CONTENU-------->





    <div class="espace"></div>
    <article id="No1" class="container_fluid">

      <h2 class="title">Présentation</h2>

      <p class="text">J'ai décidé de travailler dans le web au lycée. Comme beaucoup d'autre, le côter créatif m'a plus.
        Pour moi, un site est un des meilleur moyen de representer un concept, une idée, ou encore d'envoyer un message.
        Moi qui ne suis pas un génie du déssin, cela me permet de montrer ce qu'il y a dans ma tête, seule mes
        connaissances
        pourraient être un frein, lorsque j'ai une vision, je veux la réaliser coûte que coûte ! Bien sur, travailler
        dans le web
        c'est surtout representer les idées des autres, leur vision. Je vois ça plus comme un défi, un jeu avec des
        rêgles spécifique,
        réussir à se mettre dans l'esprit d'un autre. Mais, ce n'est pas toujours simple, c'est pour cela que j'aime
        travailler en
        équipes, partager ces connaissances, avoir un second avis, afin de ne pas se perdre en chemin dans notre
        réfléxion.
        Au final, ce que je recherche, c'est un bon environnement, des gens comme moi qui veulent faire de leur mieux,
        et s'entraident afin de s'améliorez, pour réaliser le meilleur travail possible.
      </p>

    </article>


    <article id="No2" class="container_fluid">

      <h2 class="title">Expériences</h2>

      <section class="timeline">
        <ol>
          <li>
            <div>
              <time>2017</time>
              Travaux de Manutention pour <bold>KOBA</bold> l'elections présidentiel.
            </div>
          </li>
          <li>
            <div>
              <time>2017</time> Operateur de production à Delta Dore.
            </div>
          </li>
          <li>
            <div>
              <time>2018</time> Operateur de Production à Sandem.
            </div>
          </li>
          <li>
            <div>
              <time>2018</time> Operateur de production à Canon Bretagne.
            </div>
          </li>
          <li>
            <div>
              <time>2019</time> CDD de 3 mois à CLUB ELECTRONICS en tant qu'assistant de gestion des stocks.
            </div>
          </li>
          <li>
            <div>
              <time>2019</time> Operateur de production à PSA.
            </div>
          </li>


          <li></li>
        </ol>

      </section>



    </article>

    <article id="No3" class="container_fluid">
      <script src="competence.js"></script>

      <h2 class="title">Compétences</h2>
      <div class="competence">

        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
          HTML5
        </div>

        <div class="semi-donut-model-2 margin" style="--percentage : 60; --fill: #039BE5 ;">
          CSS3
        </div>

        <div class="semi-donut-model-2 margin" style="--percentage : 35; --fill: #dae726 ;">
          JS
        </div>

        <div class="semi-donut-model-2 margin" style="--percentage : 30; --fill: #b165d4 ;">
          PHP
        </div>

      </div>


    </article>

    <article id="No4" class="container_fluid">

      <h2 class="title">Formations</h2>

      <ul>
        <li>2019 Formation Développeur Applications
          Hybride au Buroscope à Cesson-Cevigné</li>
        <li>2014/2016 BTS SNEC Système Numériques:
          Electronique et Communication
          au Lycée Alfred Kastler</li>
        <li>2012/2014 BAC STI2D Science et Technique
          de L’Industrie et Développement Durable au
          lycée Gustave Eiffel à Bordeaux</li>
      </ul>

    </article>

    <article id="No5" class="container_fluid">

      <h2 class="title">Projets</h2>
      <a href="https://mrkrokrodile.gitlab.io/pokedex2leretour/">

        <div class="projets" style="justify-content: center;">
          <article class="article_projets">
            <span class="titre">Pokedex</span>
            <img class="img_projet" src="img/pokemon.jpg" alt="pokedex">
          </article>
      </a>


      <a href="https://emeraudegestionprivee.planethoster.world/wp/">
        <article class="article_projets">
          <span class="titre">Emeraude Gestion Privée</span>
          <img class="img_projet" src="img/emeraude.jpg" alt="emeraude">
        </article>

      </a>

      <a href="https://www.figma.com/file/ERBikklrdexSHLREXL5e6r/Projet-de-Banque-de-Maillettes?node-id=0%3A1">
        <article class="article_projets">
          <span class="titre">Projet Maillette</span>
          <img class="img_projet" src="img/maillettes2.jpg" alt="maillettes">
        </article>
      </a>

      <a href="https://scratch.mit.edu/projects/435260746">
        <article class="article_projets">
          <span class="titre">Jeu Scratch</span>
          <img class="img_projet" src="img/scratch.png" alt="jeu">
        </article>
      </a>
      </div>

    </article>

  </main>

  <footer id="No6" class="container">

    <script src="https://formspree.io/js/formbutton-v1.min.js" defer></script>
    <script src="https://kit.fontawesome.com/b75c186de2.js" crossorigin="anonymous"></script>
    <script>
      window.formbutton = window.formbutton || function () {
        (formbutton.q = formbutton.q || []).push(arguments)
      };
      formbutton("create", {
        action: "https://formspree.io/f/mdopywrl",
        buttonImg: "<i class='fas fa-rocket' style='font-size:24px'/>", // <-- new button icon
        title: "🚀 Contactez moi !",
        fields: [{
            type: "email",
            label: "Email:",
            name: "email",
            required: true,
            placeholder: "votre@email.com"
          },
          {
            type: "textarea",
            label: "Message:",
            name: "message",
            placeholder: "Votre message",
          },
          {
            type: "submit"
          }
        ],

        styles: {
          fontFamily: '"Audiowide", cursive',
          button: {
            background: "#39446D",
          },

          title: {
            background: "#39446D",
            letterSpacing: "0.05em",
            textTransform: "uppercase"

          },
        },
        initiallyVisible: false

      })
    </script>




  </footer>


<?php
include "form.php";
?>


  <!-- </div> -->


  <!-- Mon script du menu -->

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
  </script>

  <script src="menu.js"></script>

</body>

</html>